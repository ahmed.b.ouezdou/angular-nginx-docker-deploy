FROM nginx:latest

ARG SRC_DIR=dist/angular-nginx-docker-deploy

COPY $SRC_DIR /usr/share/nginx/html
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
